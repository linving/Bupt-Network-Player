package com.byr.tools;

import com.byr.byrplayer.MainpageActivity;
import com.byr.byrplayer.R;
import com.byr.database.ByrPlayerDatabaseHelper;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorTreeAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class TvAdapter extends CursorTreeAdapter {

	public TvAdapter(Cursor cursor, Context context) {
		// TODO Auto-generated constructor stub
		super(cursor, context,true);
	}

	@Override
	protected void bindGroupView(View view, Context context, Cursor cursor,
			boolean isExpanded) {
		// TODO Auto-generated method stub
		String tvcatname = cursor.getString(cursor.getColumnIndex(ByrPlayerDatabaseHelper.TV_CAT_NAME));
		String tvcatdesc = cursor.getString(cursor.getColumnIndex(ByrPlayerDatabaseHelper.TV_CAT_DESC));
		int tvcatimgid = cursor.getInt(cursor.getColumnIndex(ByrPlayerDatabaseHelper.TV_CAT_IMG_ID));
		
		TextView radioName = (TextView) view.findViewById(R.id.ItemTitle);
		TextView radioDetial = (TextView) view.findViewById(R.id.ItemText);
		ImageView image = (ImageView)view.findViewById(R.id.ItemImage);
		
		if(tvcatname != null)
			radioName.setText(tvcatname);
		if(tvcatdesc != null )
			radioDetial.setText(tvcatdesc);
		
		if(tvcatimgid!=0)
			image.setBackgroundResource(R.drawable.feng);
		else
			image.setBackgroundResource(R.drawable.feng);
		
	}

	@Override
	protected Cursor getChildrenCursor(Cursor groupCursor) {
		// TODO Auto-generated method stub
		String tvcatname = groupCursor.getString(groupCursor.getColumnIndex(ByrPlayerDatabaseHelper.TV_CAT_NAME));
		return MainpageActivity.databaseHelper.getTvSets(tvcatname);
	}

	@Override
	protected View newChildView(Context context, Cursor cursor,
			boolean isLastChild, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflate=LayoutInflater.from(context);
		View view=inflate.inflate(R.layout.layout_tvset_items, null);
		bindChildView(view, context, cursor, isLastChild);
		return view;
	}

	@Override
	protected View newGroupView(Context context, Cursor cursor,
			boolean isExpanded, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflate=LayoutInflater.from(context);
		View view=inflate.inflate(com.byr.byrplayer.R.layout.layout_tvcat_items, null);
		bindGroupView(view, context, cursor, isExpanded);
		return view;
	}
	
	@Override
	protected void bindChildView(View view, Context context, Cursor cursor,
			boolean isLastChild) {
		// TODO Auto-generated method stub
		String tvsetname = cursor.getString(cursor.getColumnIndex(ByrPlayerDatabaseHelper.TV_SET_NAME));
		String tvsetdesc = cursor.getString(cursor.getColumnIndex(ByrPlayerDatabaseHelper.TV_SET_DESCR));
		int tvsetimgid = cursor.getInt(cursor.getColumnIndex(ByrPlayerDatabaseHelper.TV_SET_IMG_ID));
		
		TextView radioName = (TextView) view.findViewById(R.id.ItemTitle);
		TextView radioDetial = (TextView) view.findViewById(R.id.ItemText);
		ImageView image = (ImageView)view.findViewById(R.id.ItemImage);
		
		if(tvsetname != null)
			radioName.setText(tvsetname);
		if(tvsetdesc != null )
			radioDetial.setText(tvsetdesc);
		
		if(tvsetimgid!=0)
			image.setBackgroundResource(R.drawable.feng);
		else
			image.setBackgroundResource(R.drawable.feng);
	}
}
