package com.byr.tools;

import com.byr.byrplayer.R;
import com.byr.database.ByrPlayerDatabaseHelper;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

public class RecommendAdapter extends CursorAdapter{

	public RecommendAdapter( Cursor c,Context context) {
		// TODO Auto-generated constructor stub
		super(context, c, false);
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		// TODO Auto-generated method stub
		String name = cursor.getString(cursor.getColumnIndex(ByrPlayerDatabaseHelper.TV_SET_NAME));
		String detial = cursor.getString(cursor.getColumnIndex(ByrPlayerDatabaseHelper.TV_SET_DESCR));
		String url = cursor.getString(cursor.getColumnIndex(ByrPlayerDatabaseHelper.TV_SET_URL));
		String cat = 	cursor.getString(cursor.getColumnIndex(ByrPlayerDatabaseHelper.TV_SET_CAT_NAME));
		
		TextView radioName = (TextView) view.findViewById(R.id.ItemTitle);
		TextView radioDetial = (TextView) view.findViewById(R.id.ItemText);
		TextView radioUrl = (TextView) view.findViewById(R.id.url);
		TextView radioCat = (TextView) view.findViewById(R.id.Itemcat);
		if(name != null)
			radioName.setText(name);
		if(detial != null )
			radioDetial.setText(detial);
		if(url != null )
			radioUrl.setText(url);
		if(cat != null)
			radioCat.setText(cat);
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		// TODO Auto-generated method stub
		LayoutInflater inflate = LayoutInflater.from(context);
		View view = inflate.inflate(R.layout.layout_recommend, null);
		bindView(view, context, cursor);
		return view;
	}

}
