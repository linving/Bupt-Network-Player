package com.byr.byrplayer;

import com.byr.tools.TvAdapter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;

public class TvFragment extends Fragment{
	static MainpageActivity parent;
	View fv;
	public TvFragment(MainpageActivity m){
		parent = m;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		fv = inflater.inflate(R.layout.fragment_tv, null); 
		ExpandableListView expandableListView =(ExpandableListView)fv.findViewById(R.id.list);
        TvAdapter adapter_tvcat = new TvAdapter(MainpageActivity.databaseHelper.getAllTvCats(),parent);
        //在这里设置了适配器
        expandableListView.setAdapter(adapter_tvcat);
        expandableListView.setOnChildClickListener(new OnChildClickListener() {

            @Override
            public boolean onChildClick(ExpandableListView parent, View v,int groupPosition, int childPosition, long id) {
            		
            		Intent intent = new Intent(TvFragment.parent,VideoActivity.class);
            		Bundle bundle=new Bundle();  
            		if(groupPosition == 0 && childPosition == 4){
            			bundle.putString("url","rtsp://10.3.220.249/CCTV-5");  
            		}else if(groupPosition == 2 && childPosition == 3){
            			bundle.putString("url","http://live.3gv.ifeng.com/zhongwen.m3u8");  
            		}else if(groupPosition == 2 && childPosition == 4){
            			bundle.putString("url","rtmp://10.3.18.186:1935/vod/BillGates.mp4");  
            		}else{
            			bundle.putString("url","http://live.3gv.ifeng.com/zixun.m3u8");  
            		}
            		intent.putExtras(bundle);  
            		startActivity(intent);
                	return false;
            }
        });
		return fv;
	}
}
