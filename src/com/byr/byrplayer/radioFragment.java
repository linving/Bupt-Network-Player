package com.byr.byrplayer;

import com.byr.network.ConnectionServer;
import com.byr.tools.RadioAdapter;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnLastItemVisibleListener;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import android.support.v4.app.Fragment;
import android.text.format.DateUtils;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class radioFragment extends Fragment{
	MainpageActivity parent;
	public MediaPlayer currentplay = null;
	public RadioAdapter radioAdapter;
	public ListView list;
	private PullToRefreshListView mPullRefreshListView;
	View fv;
	public boolean mediaplaying = false;
	public radioFragment(MainpageActivity m){
		parent = m;
	}
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		fv = inflater.inflate(R.layout.fragment_list, null);
		mPullRefreshListView = (PullToRefreshListView)fv.findViewById(R.id.ListView01);
		// Set a listener to be invoked when the list should be refreshed.
				mPullRefreshListView.setOnRefreshListener(new OnRefreshListener<ListView>() {
					@Override
					public void onRefresh(PullToRefreshBase<ListView> refreshView) {
						String label = DateUtils.formatDateTime(parent.getApplicationContext(), System.currentTimeMillis(),
								DateUtils.FORMAT_SHOW_TIME | DateUtils.FORMAT_SHOW_DATE | DateUtils.FORMAT_ABBREV_ALL);

						// Update the LastUpdatedLabel
						refreshView.getLoadingLayoutProxy().setLastUpdatedLabel(label);

						// Do work to refresh the list here.
						new GetDataTask().execute();
					}
				});

				// Add an end-of-list listener
				mPullRefreshListView.setOnLastItemVisibleListener(new OnLastItemVisibleListener() {

					@Override
					public void onLastItemVisible() {
						
					}
				});

		list = mPullRefreshListView.getRefreshableView(); 
		radioAdapter = new RadioAdapter(parent,MainpageActivity.databaseHelper.getAllRadioSets(),true);
        //添加并且显示
        list.setAdapter(radioAdapter);
        //添加点击
        list.setOnItemClickListener(new OnItemClickListener(){

		@Override
			public void onItemClick(AdapterView<?> arg0, View view, int arg2,long arg3) {
				// TODO Auto-generated method stub
				Log.d("list","begin");
			 	TextView urlText = (TextView)view.findViewById(R.id.url);
				String url =urlText.getText().toString();
				String name = ((TextView)view.findViewById(R.id.ItemTitle)).getText().toString();
				String desc = ((TextView)view.findViewById(R.id.ItemText)).getText().toString();
				((TextView)fv.findViewById(R.id.radioname)).setText(name.toCharArray(), 0, name.length());
				((TextView)fv.findViewById(R.id.radiodesc)).setText(desc.toCharArray(), 0, desc.length());
				Log.d("list",url);
				MediaPlayer mp = new MediaPlayer();
				mp.setOnCompletionListener(new OnCompletionListener(){
					@Override
					public void onCompletion(MediaPlayer mp) {
						// TODO Auto-generated method stub
						((ImageView)fv.findViewById(R.id.operate)).setBackground(parent.getResources().getDrawable(R.drawable.play));
						mediaplaying = false;
					}
				});
				if(currentplay!=null && currentplay.isPlaying())
					currentplay.stop();
				currentplay = mp;
				try {
					Uri uriprase = Uri.parse(url);
					mp.setDataSource(parent,uriprase);
					showToask("a");
					mp.prepare();
					showToask("b");
					mp.start();
					showToask("c");
					((ImageView)fv.findViewById(R.id.operate)).setBackground(parent.getResources().getDrawable(R.drawable.pause));
					mediaplaying = true;
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					currentplay = null;
					mediaplaying = false;
					((ImageView)fv.findViewById(R.id.operate)).setBackground(parent.getResources().getDrawable(R.drawable.play));
				}
				
				
			}
    	   
       });
        ((ImageView)fv.findViewById(R.id.operate)).setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View view) {
				// TODO Auto-generated method stub
				try{
					if(mediaplaying){
						if(currentplay != null){
							currentplay.pause();
							mediaplaying = false;
							((ImageView)fv.findViewById(R.id.operate)).setBackground(parent.getResources().getDrawable(R.drawable.play));
						}
					}else{
						if(currentplay != null){
							currentplay.start();
							mediaplaying = true;
							((ImageView)fv.findViewById(R.id.operate)).setBackground(parent.getResources().getDrawable(R.drawable.pause));
						}
					}
				}catch(Exception e){
					
				}
			}
        	
        });
		return fv;
	}
	
	private class GetDataTask extends AsyncTask<Void, Void, String[]> {

		@Override
		protected String[] doInBackground(Void... params) {
			// Simulates a background job.
			try {
				MainpageActivity.refreshed = false;
				new Thread(){
					public void run(){
						ConnectionServer.requireRadioUpdate();
					}
				}.start();
				for(int i = 0;i < 50 && MainpageActivity.refreshed == false;i++)
					Thread.sleep(100);
				if(MainpageActivity.refreshed){
					Message msg = new Message();  
    	            msg.what = -1;  
    	            MainpageActivity.handler.sendMessage(msg);  
				}else{
					Message msg = new Message();  
    	            msg.what = -2;  
    	            MainpageActivity.handler.sendMessage(msg); 
				}
			} catch (InterruptedException e) {
			}
			return null;
		}

		@Override
		protected void onPostExecute(String[] result) {
			// Call onRefreshComplete when the list has been refreshed.
			mPullRefreshListView.onRefreshComplete();

			super.onPostExecute(result);
		}
	}
	
	void showToask(String hint) {
		Toast toast = Toast.makeText(this.getActivity(), hint, Toast.LENGTH_SHORT);
		toast.show();
	}
}
