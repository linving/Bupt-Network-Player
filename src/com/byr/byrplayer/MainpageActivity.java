package com.byr.byrplayer;

import java.util.ArrayList;

import com.byr.database.ByrPlayerDatabaseHelper;
import com.byr.network.ConnectionServer;
import com.special.ResideMenu.ResideMenu;
import com.special.ResideMenu.ResideMenuItem;

import android.annotation.TargetApi;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class MainpageActivity extends FragmentActivity{
	
	android.support.v4.app.Fragment mContent;
	public static MainpageActivity mcontext;
	public static FirstFragment firstfragment;
	public static radioFragment radiofragment;
	public static TvFragment tvfragment;
	private static final String TAG = "MainActivity";
    private ViewPager mPager;
    private ArrayList<Fragment> fragmentsList;
    private ImageView ivBottomLine;
    private TextView tvTabActivity, tvTabGroups, tvTabFriends, tvTabChat;
    public static String addUrl[] = null;
    private int currIndex = 0;
    private int bottomLineWidth;
    private int offset = 0;
    private int position_one;
    private int position_two;
    private int position_three;
    private Resources resources;
	public static ByrPlayerDatabaseHelper databaseHelper = null; 
	public static Handler handler;
	public static boolean refreshed = true;
	public boolean isScrolling;
	public View ignored;
	public View adview;
	public boolean ignoreviewadded = false;
	public boolean adinignore = false;
	ResideMenu resideMenu;
	public int page = 0;
	public enum modeSelect {
        Radiosel,Tvsel;    
    }
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		if (!io.vov.vitamio.LibsChecker.checkVitamioLibs(this))
			return;
		setContentView(R.layout.activity_mainpage);
		
		databaseHelper = new ByrPlayerDatabaseHelper(this);
		databaseHelper.getReadableDatabase();
    	
		firstfragment= new FirstFragment(this);
		radiofragment= new radioFragment(this);
		tvfragment = new TvFragment(this);
		mContent = firstfragment;

		resources = getResources();
        InitWidth();
        InitTextView();
        InitViewPager();
        handler = new Handler() {  
            @Override  
            public void handleMessage(Message msg) {  
                if (msg.what == 1) {  
                	MainpageActivity.radiofragment.radioAdapter.changeCursor(MainpageActivity.databaseHelper.getAllRadioSets());  
                	refreshed = true;
                }else if (msg.what == -1) {  
                	showToask("电台更新成功，Byrer们祝你有个好心情");
                }else if (msg.what == -2) {  
                	showToask("一会儿再试吧，没有收到数据呢。。。");
                }else if(msg.what == -10){
                	adUrlRefresh();
                }else if(msg.what == 10){
                	if(!adinignore && page == 0){
                		resideMenu.addIgnoredView(firstfragment.adview);
                		adinignore = true;
                	}
                }else if(msg.what == 11){
                	if(adinignore){
                		resideMenu.removeIgnoredView(firstfragment.adview);
                		adinignore = false;
                	}
                }
            }  
        };  
     // attach to current activity;
        resideMenu = new ResideMenu(this);
        resideMenu.setBackground(R.drawable.menu_background);
        resideMenu.attachToActivity(this);

        // create menu items;
        String titles[] = { "Home", "Profile", "Calendar", "Settings" };
        int icon[] = { R.drawable.icon_home, R.drawable.icon_profile, R.drawable.icon_calendar, R.drawable.icon_settings };

        for (int i = 0; i < titles.length; i++){
            ResideMenuItem item = new ResideMenuItem(this, icon[i], titles[i]);
            //item.setOnClickListener(this);
            resideMenu.addMenuItem(item,  ResideMenu.DIRECTION_LEFT); // or  ResideMenu.DIRECTION_RIGHT
        }
        resideMenu.setSwipeDirectionDisable(ResideMenu.DIRECTION_RIGHT);
        //resideMenu.addIgnoredView(this.findViewById(R.id.vPager));
        ignored = this.findViewById(R.id.vPager);
        new Thread(){
        	public void run(){
        			ConnectionServer.sendHello();
        			ConnectionServer.requireRadioUpdate();
        			ConnectionServer.requireAd();
        	}
        }.start();

	}
	
	@Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return resideMenu.dispatchTouchEvent(ev);
    }
	
	private void InitTextView() {
        tvTabActivity = (TextView) findViewById(R.id.tv_tab_activity);
        tvTabGroups = (TextView) findViewById(R.id.tv_tab_groups);
        tvTabFriends = (TextView) findViewById(R.id.tv_tab_friends);
        tvTabChat = (TextView) findViewById(R.id.tv_tab_chat);

        tvTabActivity.setOnClickListener(new MyOnClickListener(0));
        tvTabGroups.setOnClickListener(new MyOnClickListener(1));
        tvTabFriends.setOnClickListener(new MyOnClickListener(2));
        tvTabChat.setOnClickListener(new MyOnClickListener(3));
    }
	private void InitViewPager() {
        mPager = (ViewPager) findViewById(R.id.vPager);
        fragmentsList = new ArrayList<Fragment>();
            
        fragmentsList.add(firstfragment);
        fragmentsList.add(radiofragment);
        fragmentsList.add(tvfragment);
        fragmentsList.add(new Fragment());
        
        mPager.setAdapter(new MyFragmentPagerAdapter(getSupportFragmentManager(), fragmentsList));
        mPager.setCurrentItem(0);
        mPager.setOnPageChangeListener(new MyOnPageChangeListener());
        mPager.setOffscreenPageLimit(4);  
    }

    private void InitWidth() {
        ivBottomLine = (ImageView) findViewById(R.id.iv_bottom_line);
        bottomLineWidth = ivBottomLine.getLayoutParams().width;
        Log.d(TAG, "cursor imageview width=" + bottomLineWidth);
        DisplayMetrics dm = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(dm);
        int screenW = dm.widthPixels;
        offset = (int) ((screenW / 4.0 - bottomLineWidth) / 2);
        Log.i("MainActivity", "offset=" + offset);

        position_one = (int) (screenW / 4.0);
        position_two = position_one * 2;
        position_three = position_one * 3;
    }
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}
	public class MyOnClickListener implements View.OnClickListener {
        private int index = 0;

        public MyOnClickListener(int i) {
            index = i;
        }

        @Override
        public void onClick(View v) {
            mPager.setCurrentItem(index);
        }
    };

    public class MyOnPageChangeListener implements OnPageChangeListener {

        @Override
        public void onPageSelected(int arg0) {
            Animation animation = null;
            switch (arg0) {
            case 0:
                if (currIndex == 1) {
                    animation = new TranslateAnimation(position_one, 0, 0, 0);
                    tvTabGroups.setTextColor(resources.getColor(R.color.lightwhite));
                } else if (currIndex == 2) {
                    animation = new TranslateAnimation(position_two, 0, 0, 0);
                    tvTabFriends.setTextColor(resources.getColor(R.color.lightwhite));
                } else if (currIndex == 3) {
                    animation = new TranslateAnimation(position_three, 0, 0, 0);
                    tvTabChat.setTextColor(resources.getColor(R.color.lightwhite));
                }
                tvTabActivity.setTextColor(resources.getColor(R.color.white));
                break;
            case 1:
                if (currIndex == 0) {
                    animation = new TranslateAnimation(0, position_one, 0, 0);
                    tvTabActivity.setTextColor(resources.getColor(R.color.lightwhite));
                } else if (currIndex == 2) {
                    animation = new TranslateAnimation(position_two, position_one, 0, 0);
                    tvTabFriends.setTextColor(resources.getColor(R.color.lightwhite));
                } else if (currIndex == 3) {
                    animation = new TranslateAnimation(position_three, position_one, 0, 0);
                    tvTabChat.setTextColor(resources.getColor(R.color.lightwhite));
                }
                tvTabGroups.setTextColor(resources.getColor(R.color.white));
                break;
            case 2:
                if (currIndex == 0) {
                    animation = new TranslateAnimation(0, position_two, 0, 0);
                    tvTabActivity.setTextColor(resources.getColor(R.color.lightwhite));
                } else if (currIndex == 1) {
                    animation = new TranslateAnimation(position_one, position_two, 0, 0);
                    tvTabGroups.setTextColor(resources.getColor(R.color.lightwhite));
                } else if (currIndex == 3) {
                    animation = new TranslateAnimation(position_three, position_two, 0, 0);
                    tvTabChat.setTextColor(resources.getColor(R.color.lightwhite));
                }
                tvTabFriends.setTextColor(resources.getColor(R.color.white));
                break;
            case 3:
                if (currIndex == 0) {
                    animation = new TranslateAnimation(0, position_three, 0, 0);
                    tvTabActivity.setTextColor(resources.getColor(R.color.lightwhite));
                } else if (currIndex == 1) {
                    animation = new TranslateAnimation(position_one, position_three, 0, 0);
                    tvTabGroups.setTextColor(resources.getColor(R.color.lightwhite));
                } else if (currIndex == 2) {
                    animation = new TranslateAnimation(position_two, position_three, 0, 0);
                    tvTabFriends.setTextColor(resources.getColor(R.color.lightwhite));
                }
                tvTabChat.setTextColor(resources.getColor(R.color.white));
                break;
            }
            //showToask(String.valueOf(arg0));
            page = arg0;
        	if(arg0 == 0){
        		if(ignoreviewadded){
        			resideMenu.removeIgnoredView(ignored);
        			//resideMenu.addIgnoredView(tvfragment.adview);
        			//resideMenu.addIgnoredView(ignored);
        			ignoreviewadded = false;
        		}
        		Message msg = new Message();  
	            msg.what = 10;  
	            MainpageActivity.handler.sendMessage(msg);  
            }else{
            	if(!ignoreviewadded){
            		resideMenu.addIgnoredView(ignored);
            		//resideMenu.removeIgnoredView(tvfragment.adview);
            		ignoreviewadded = true;
            	}
            	Message msg = new Message();  
	            msg.what = 11;  
	            MainpageActivity.handler.sendMessage(msg);  
            }
            currIndex = arg0;
            animation.setFillAfter(true);
            animation.setDuration(300);
            ivBottomLine.startAnimation(animation);
        }

        @Override
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        	 
        }

        @Override
        public void onPageScrollStateChanged(int arg0) {
        	
        }
    }
    
    void showToask(String hint) {
		Toast toast = Toast.makeText(this, hint, Toast.LENGTH_SHORT);
		toast.show();
	}
    
    void adUrlRefresh(){
    	firstfragment.changePageAdapter();
    }
}
