package com.byr.network;

import org.apache.mina.core.service.IoHandlerAdapter;
import org.apache.mina.core.session.IoSession;

import com.byr.byrplayer.MainpageActivity;
import com.byr.byrplayer.R;
import com.byr.byrplayer.FirstFragment;
import com.byr.network.RadioInfo.RadioSetInfo;


import android.os.Message;
import android.util.Log;

public class ClientHandler extends IoHandlerAdapter {
	 	public void messageReceived(IoSession session, Object message) throws Exception { 
	        System.out.println("收到服务器消息：" + message.toString()); 
	        Log.d("netlink","收到服务器消息：" + " 处理中。。");
	        if(message instanceof ReplyBody){
	        	ReplyBody replybody = (ReplyBody)message;
	        	Log.d("netlink","Kind: " + replybody.REPLY_TYEP);
	        	if(replybody.REPLY_TYEP == ReplyBody.TYPE_HELLO_TOO){
	        		Log.d("netlink","收到服务器问候消息");
	        	}else if(replybody.REPLY_TYEP == ReplyBody.TYPE_STATUS_REPORT){
	        		Log.d("netlink","收到服务器状态信息");
	        	}else if(replybody.REPLY_TYEP == ReplyBody.TYPE_RADIO_UPDATE){
	        		Log.d("netlink","收到服务器更新信息");
	        		if(replybody.getRadioInfo().size() != 0)
	        			MainpageActivity.databaseHelper.deleteAllRadioSet();
	        		for(int i = 0;i < replybody.getRadioInfo().size(); i ++){
	        			RadioSetInfo info = replybody.getRadioInfo().get(i);
	        			Log.d("netlink","+--"+info.name+"---"+info.desc+"---"+info.url+"--+");
	        			if(MainpageActivity.databaseHelper != null){
	        				MainpageActivity.databaseHelper.tryToInsertRadioSet(info.name, info.desc, info.url, R.drawable.feng);
	        			}
	                }
	        		try{
        				Message msg = new Message();  
        	            msg.what = 1;  
        	            MainpageActivity.handler.sendMessage(msg);  
        			}catch(Exception e){
        				Log.d("netlink",e.toString());
        			}
	        		Log.d("netlink","收到服务器更新收音机频道信息"+replybody.getRadioInfo().size());
	        		
	        	}else if(replybody.REPLY_TYEP == ReplyBody.TYPE_AD){
	        		MainpageActivity.addUrl = replybody.adUrl;
	        		try{
        				Message msg = new Message();  
        	            msg.what = -10;  
        	            FirstFragment.handler.sendMessage(msg);  
        			}catch(Exception e){
        				Log.d("netlink","handler+  "+e.toString());
        			}
	        		Log.d("netlink","收到服务器更新广告信息"+replybody.getRadioInfo().size());
	        	}
	        }else if(message instanceof SendBody){
	        	
	        }
	    } 
	 
	    public void exceptionCaught(IoSession arg0, Throwable arg1) 
	            throws Exception { 
	 
	    }
}
